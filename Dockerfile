FROM python:3

ADD main.py /
ADD requirements.txt /

RUN python3 -m pip install --upgrade pip setuptools wheel
RUN python3 -m pip install -r requirements.txt

CMD [ "python", "./main.py" ]

ENV PYTHONUNBUFFERED=1