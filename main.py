# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 16:29:38 2020

@author: ben
"""

import c2mon
import time
import logging
from inspect import currentframe, getframeinfo
#print()            gives text on console
#logging.warning    gives warning on console
#logging.error      gives error on console

#logging level
logging.basicConfig();
logging.getLogger().setLevel(logging.INFO);

ip           = '192.168.111.77'; #C2mon-Server IP
port         = '31300'; #Rest Gateway Port
#ip           = 'matlab-gw.sense-lab.net'; #OLD   C2mon-Server IP
#port         = '1412'; #OLD     Rest Gateway Port
tagPathRN    = 'RefrigerationNetwork/'; #C2mon Data Tag -> Needed to get Data from Component
tagPathRNvirt    = 'RefrigerationNetworkVirtual/'; #C2mon Data Tag -> Needed to get Data from Component
tagPathLS    = 'LoadSimulator/';     #C2mon Data Tag -> Needed to get Data from Component
tagPathACOG2 = 'AirConOG2/'; #
tagPathACEG  = 'AirConEG/'; #
tagPathC80  = 'CinergiaEL20-80/';
tagPathC81  = 'CinergiaEL20-81/';
tagPathC82  = 'CinergiaEL20-82/';
options = 'http://'+ip+':'+port; #Set Gateway Path

#save for on change
cp_gl = 3.7; #Glykol 37% at 0°C
P_KMZ1 = 0.01;
P_KMZ2 = 0.01;
P_KMZ3 = 0.01;
P_KMZ4 = 0.01;
try:
    P_KMZ5_0 = float(c2mon.get_data_raw(tagPathRN+'KMZ5_Kaelteleistung',options));
except:
    print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
P_KMZ5_1 = P_KMZ5_0;
P_KMZseriell = 0.01;
V_KMZ1 = 0.01;
V_KMZ2 = 0.01;
V_KMZ3 = 0.01;
V_KMZ4 = 0.01;
SOC_SP1_o = 0.01;
SOC_SP2_o= 0.01;
SOC_SP_o= 0.01;
SOC_SP_E_o= 0.01;
E_KMZ1_o = 0.01;
E_KMZ2_o = 0.01;
E_KMZ3_o = 0.01;
E_KMZ4_o = 0.01;
E_KMZ5_o = 0.01;
alive = 0;
set_alive =0.0;
### SOC variables ###
E_SP_max = 51; #51kWh
ES_max = 5;
ES1_O_min = -2.4;
ES1_M_min = -4;
ES1_U_min = -5;
ES2_O_min = -3.7;
ES2_M_min = -4.8;
ES2_U_min = -5;


print('############################################')
print('############# Script Start #################')
print('############################################')

while True:
        start = time.time();
        time.sleep(1); #sleep for 1s
        alive = alive +1;
        if alive>30:
            alive = 0;
            print('Script alive')
            
        try:            
            c2mon.set_data(tagPathRNvirt + 'VirtualDP_Alive', set_alive,options);
        except:
            print("ERROR: Matlab-Rest-Gateway nicht erreichbar %s", getframeinfo(currentframe()).lineno);
        set_alive +=1.0;            
    
        ##############################
        ############ KMZ1 ############
        ##############################
        try:
            T_VL = float(c2mon.get_data_raw(tagPathRN+'KMZ1_Temperatur_VL',options));
            T_RL = float(c2mon.get_data_raw(tagPathRN+'KMZ1_Temperatur_RL',options));
            Volumenstrom  = float(c2mon.get_data_raw(tagPathRN+'KMZ1_Durchfluss_direkt',options));
        except:
            print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        Volumenstrom = (Volumenstrom-2.95)*1.7;
        

        #P_th_KMZ1 = Cp/3.6 x Volumenstrom (m³/h) x (T_RL - T_VL)
        P = cp_gl/3.6 * Volumenstrom * (T_RL-T_VL);
        P = round(P,2); #round to decimal 2
        
        Volumenstrom = round(Volumenstrom,2); #round to decimal 2
        if Volumenstrom<0:
            Volumenstrom = 0;
        if Volumenstrom !=V_KMZ1: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ1_Volumenstrom_virtuell', str(Volumenstrom),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
            
        V_KMZ1 = Volumenstrom;
        
        if abs(P)<0.3:
            P = 0;
        if P !=P_KMZ1: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ1_Leistung_virtuell', str(P),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        P_KMZ1_0 = P_KMZ1;
        P_KMZ1_1 = P;
        P_KMZ1 = P;
        
        ##############################
        ############ KMZ2 ############
        ##############################
        try:
            T_VL = float(c2mon.get_data_raw(tagPathRN+'KMZ2_Temperatur_VL',options));
            T_RL = float(c2mon.get_data_raw(tagPathRN+'KMZ2_Temperatur_RL',options));
            Volumenstrom  = float(c2mon.get_data_raw(tagPathRN+'KMZ2_Durchfluss_direkt',options));
        except:
            print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        Volumenstrom = (Volumenstrom-3.045)*1.65;

        #P_th_KMZ2 = Cp/3.6 x Volumenstrom (m³/h) x (T_RL - T_VL)
        P = cp_gl/3.6 * Volumenstrom * (T_RL-T_VL);
        P = round(P,2); #round to decimal 2
                
        Volumenstrom = round(Volumenstrom,2); #round to decimal 2
        if Volumenstrom<0:
            Volumenstrom = 0;
        if Volumenstrom !=V_KMZ2: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ2_Volumenstrom_virtuell', str(Volumenstrom),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        V_KMZ2 = Volumenstrom;
        
        if abs(P)<0.3:
            P = 0;
        if P !=P_KMZ2: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ2_Leistung_virtuell', str(P),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        P_KMZ2_0 = P_KMZ2;
        P_KMZ2_1 = P;
        P_KMZ2 = P;

        ##############################
        ############ KMZ3 ############
        ##############################
        try:
            T_VL = float(c2mon.get_data_raw(tagPathRN+'KMZ3_Temperatur_VL',options));
            T_RL = float(c2mon.get_data_raw(tagPathRN+'KMZ3_Temperatur_RL',options));
            Volumenstrom  = float(c2mon.get_data_raw(tagPathRN+'KMZ3_Durchfluss_direkt',options));
        except:
            print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        Volumenstrom = (Volumenstrom-3.02)*1.65;

        #P_th_KMZ3 = Cp/3.6 x Volumenstrom (m³/h) x (T_RL - T_VL)
        P = cp_gl/3.6 * Volumenstrom * (T_RL-T_VL);
        P = round(P,2); #round to decimal 2
                
        Volumenstrom = round(Volumenstrom,2); #round to decimal 2
        if Volumenstrom<0:
            Volumenstrom = 0;
        if Volumenstrom !=V_KMZ3: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ3_Volumenstrom_virtuell', str(Volumenstrom),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        V_KMZ3 = Volumenstrom;
        
        if abs(P)<0.3:
            P = 0;
        if P !=P_KMZ3: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ3_Leistung_virtuell', str(P),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        P_KMZ3_0 = P_KMZ3;
        P_KMZ3_1 = P;
        P_KMZ3 = P;

        ##############################
        ############ KMZ4 ############
        ##############################
        try:
            T_VL = float(c2mon.get_data_raw(tagPathRN+'KMZ4_Temperatur_VL',options));
            T_RL = float(c2mon.get_data_raw(tagPathRN+'KMZ4_Temperatur_RL',options));
            Volumenstrom  = float(c2mon.get_data_raw(tagPathRN+'KMZ4_Durchfluss_direkt',options));
        except:
            print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        Volumenstrom = (Volumenstrom-2.95)*1.7;

        #P_th_KMZ4 = Cp/3.6 x Volumenstrom (m³/h) x (T_RL - T_VL)
        P = cp_gl/3.6 * Volumenstrom * (T_RL-T_VL);
        P = round(P,2); #round to decimal 2
        
        Volumenstrom = round(Volumenstrom,2); #round to decimal 2
        if Volumenstrom<0:
            Volumenstrom = 0;
        if Volumenstrom !=V_KMZ4: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ4_Volumenstrom_virtuell', str(Volumenstrom),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        V_KMZ4 = Volumenstrom;
        
        if abs(P)<0.3:
            P = 0;
        if P !=P_KMZ4: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'KMZ4_Leistung_virtuell', str(P),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        P_KMZ4_0 = P_KMZ4;
        P_KMZ4_1 = P;
        P_KMZ4 = P;

        ##############################
        ############ KMZ Seriell #####
        ##############################
        try:
            T_VL = float(c2mon.get_data_raw(tagPathRN+'KMZ2_Temperatur_VL',options));
            T_RL = float(c2mon.get_data_raw(tagPathRN+'KMZ3_Temperatur_VL',options));
        except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        
        Volumenstrom  = V_KMZ1;

        #P_th_KMZ4 = Cp/3.6 x Volumenstrom (m³/h) x (T_RL - T_VL)
        P = cp_gl/3.6 * Volumenstrom * (T_RL-T_VL);
        P = round(P,2); #round to decimal 2
        
        if abs(P)<0.3:
            P = 0;
        if P !=P_KMZseriell: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'P_KMZseriell_Leistung_virtuell', str(P),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        P_KMZseriell_0 = P_KMZseriell;
        P_KMZseriell_1 = P;
        P_KMZseriell = P;
        
        ##############################
        ####### SOC Speicher #########
        ##############################
        
        try:
            ES1_O = float(c2mon.get_data_raw(tagPathRN+'KS1_Temperatur_Oben',options));
            ES1_M = float(c2mon.get_data_raw(tagPathRN+'KS1_Temperatur_Mitte',options));
            ES1_U = float(c2mon.get_data_raw(tagPathRN+'KS1_Temperatur_Unten',options));
            ES2_O = float(c2mon.get_data_raw(tagPathRN+'KS2_Temperatur_Oben',options));
            ES2_M = float(c2mon.get_data_raw(tagPathRN+'KS2_Temperatur_Mitte',options));
            ES2_U = float(c2mon.get_data_raw(tagPathRN+'KS2_Temperatur_Unten',options));
        except:
            print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        
        
        
        SOC_ES1_O = (ES_max-ES1_O)/(ES_max-ES1_O_min)*100;
        SOC_ES1_M = (ES_max-ES1_M)/(ES_max-ES1_U_min)*100;
        SOC_ES1_U = (ES_max-ES1_U)/(ES_max-ES1_M_min)*100;
        SOC_ES2_O = (ES_max-ES2_O)/(ES_max-ES2_O_min)*100;
        SOC_ES2_M = (ES_max-ES2_M)/(ES_max-ES2_U_min)*100;
        SOC_ES2_U = (ES_max-ES2_U)/(ES_max-ES2_M_min)*100;
        
        SOC_SP1 = (SOC_ES1_O+SOC_ES1_M+SOC_ES1_U)/3;
        SOC_SP2 = (SOC_ES2_O+SOC_ES2_M+SOC_ES2_U)/3;
        SOC_SP = SOC_SP1 + SOC_SP2;
        SOC_SP1 = round(SOC_SP1,2);
        SOC_SP2 = round(SOC_SP2,2);
        SOC_SP = round(SOC_SP,2);
        
        if SOC_SP1 !=SOC_SP1_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'SOC_SP1', str(SOC_SP1) ,options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if SOC_SP2 !=SOC_SP2_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'SOC_SP2', str(SOC_SP2) ,options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if SOC_SP !=SOC_SP_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'SOC_SP_Temp', str(SOC_SP) ,options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
   
        ##############################
        ####### Energy KMZ ###########
        ##############################
        
        if (c2mon.get_data_raw(tagPathRNvirt+'E_KMZ1_reset',options) == true):
            E_KMZ1_o = 0;
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZ1_reset', bool(0),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        else:
        
        try:
            E_KMZ1_o = float(c2mon.get_data_raw(tagPathRNvirt+'E_KMZ1',options));
            E_KMZ2_o = float(c2mon.get_data_raw(tagPathRNvirt+'E_KMZ2',options));
            E_KMZ3_o = float(c2mon.get_data_raw(tagPathRNvirt+'E_KMZ3',options));
            E_KMZ4_o = float(c2mon.get_data_raw(tagPathRNvirt+'E_KMZ4',options));
            E_KMZ5_o = float(c2mon.get_data_raw(tagPathRNvirt+'E_KMZ5',options));
            E_KMZseriell_o = float(c2mon.get_data_raw(tagPathRNvirt+'E_KMZseriell',options));
            P_KMZ5_0 = P_KMZ5_1;
            P_KMZ5_1 = float(c2mon.get_data_raw(tagPathRN+'KMZ5_Kaelteleistung',options));
        except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
        
        resulution = (time.time()-start)/60/60; # Script execute time till now ||| kWh -> hours broken to Seconds with /60/60
        
        
        
        E_KMZ1 = (P_KMZ1_0 + P_KMZ1_1)/2*resulution + E_KMZ1_o;
        E_KMZ2 = (P_KMZ2_0 + P_KMZ2_1)/2*resulution + E_KMZ2_o;
        E_KMZ3 = (P_KMZ3_0 + P_KMZ3_1)/2*resulution + E_KMZ3_o;
        E_KMZ4 = (P_KMZ4_0 + P_KMZ4_1)/2*resulution + E_KMZ4_o;
        E_KMZ5 = (P_KMZ5_0 + P_KMZ5_1)/2*resulution + E_KMZ5_o;
        E_KMZseriell = (P_KMZseriell_0 + P_KMZseriell_1)/2*resulution + E_KMZseriell_o;
        SOC_SP_E = E_KMZ2 / E_SP_max;
        #E_KMZ1 = round(E_KMZ1,2);
        #E_KMZ2 = round(E_KMZ2,2);
        #E_KMZ3 = round(E_KMZ3,2);
        #E_KMZ4 = round(E_KMZ4,2);
        SOC_SP_E = round(SOC_SP_E,2);
        if E_KMZ1 !=E_KMZ1_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZ1', str(E_KMZ1),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if E_KMZ2 !=E_KMZ2_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZ2', str(E_KMZ2),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if E_KMZ3 !=E_KMZ3_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZ3', str(E_KMZ3),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if E_KMZ4 !=E_KMZ4_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZ4', str(E_KMZ4),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if E_KMZ5 !=E_KMZ5_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZ5', str(E_KMZ5),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        if E_KMZseriell !=E_KMZseriell_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'E_KMZseriell', str(E_KMZseriell),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)       
                
        if SOC_SP_E !=SOC_SP_E_o: #on change
            try:
                c2mon.set_data(tagPathRNvirt + 'SOC_SP_E', str(SOC_SP_E),options);
            except:
                print("ERROR: Matlab-Rest-Gateway nicht erreichbar at line", getframeinfo(currentframe()).lineno)
                
        #logging.info("Script execute Time: %s", time.time()-start);